import React from "react"
import vibrant from "node-vibrant"
import Toolbar from "./Toolbar"
import Progress from "./Progress";

const styles = require("~assets/components/player.scss")

interface Props {
    cover: string
    audio: string
    onSkip?: () => void
    onPrevious?: () => void
    onEnded?: () => void
    hasNextSong?: boolean
    hasPreviousSong?: boolean
}

interface State {
    durationTime: number
    currentTime: number
    playing: boolean
}

export default class Player extends React.PureComponent<Props, State> {

    public state: State = {
        playing: false,
        durationTime: NaN,
        currentTime: NaN,
    }

    public componentDidMount() {
        this.getColor()
    }

    private getColor() {
        const { cover } = this.props;
        vibrant.from(cover).getPalette().then((palette) => {
            this.container.current!.style.backgroundImage = `linear-gradient(${palette.LightVibrant!.hex}, ${palette.DarkVibrant!.hex})`
        })
    }

    private container = React.createRef<HTMLElement>()
    private audio = React.createRef<HTMLAudioElement>()

    private handlePause = () => {
        this.setState({ playing: false }, () => {
            this.audio.current!.pause();
        })
    }

    private handleEnded = () => {
        this.props.onEnded && this.props.onEnded()
    }

    private handlePlay = () => {
        this.setState({ playing: true }, () => {
            this.audio.current!.play();
        })
    }

    public componentDidUpdate(prevProps: Props) {
        if (prevProps.cover !== this.props.cover) {
            this.getColor() 
        }
        
        if (prevProps.audio !== this.props.audio) {
            const audio = this.audio.current!
            audio.pause()
            audio.load()
            if (this.state.playing) {
                audio.play()
            }
        }
    }

    private loadSongMetadata = () => {
        const audio = this.audio.current!
        this.setState({
            durationTime: Math.round(audio.duration),
            currentTime: Math.round(audio.currentTime)
        })
    }

    private handleProgressClick = (percentage: number) => {
        const { durationTime } = this.state
        const audio = this.audio.current!
        const timestamp = Math.round(durationTime * (percentage / 100))
        audio.currentTime = timestamp
    }

    public render() {
        return (
            <section className={styles.container} ref={this.container}>
                <section className={styles.albumCover}>
                    <img role="album cover" className={styles.image} src={this.props.cover} />
                </section>
                <audio
                    ref={this.audio}
                    onLoadedMetadata={this.loadSongMetadata}
                    onTimeUpdate={this.loadSongMetadata}
                    onEnded={this.handleEnded}
                >
                    <source src={this.props.audio} type="audio/mpeg"/>
                </audio>
                <Toolbar
                    playing={this.state.playing}
                    onPause={this.handlePause}
                    onPlay={this.handlePlay}
                    onSkip={this.props.onSkip}
                    onPrevious={this.props.onPrevious}
                    hasPreviousSong={this.props.hasPreviousSong}
                    hasNextSong={this.props.hasNextSong}
                />
                <Progress
                    current={this.state.currentTime}
                    duration={this.state.durationTime}
                    onClick={this.handleProgressClick}
                />
                <span>Made by Agustín González, <a href="https://gitlab.com/agustingonzalezmurua/html-5-react-web-player/tree/master">source</a></span>
            </section>
        )
    }
}