import React from "react"
import Player from "./Player"
import SongList from "./SongList";

interface Props {
    /** Enables next song reproduction after current one ends (if there is one in the queue)*/
    autoplay?: boolean
    /** Song queue */
    songs: string[]
}

interface State {
    currentSongIndex: number
    /** Current song information */
    currentSong?: { audio: string, cover: string }
    ready: boolean
}

export default class SongPlayer extends React.PureComponent<Props, State> {

    public state: State = {
        currentSongIndex: 0,
        currentSong: undefined,
        ready: false,
    }

    private static defaultProps: Props = {
        autoplay: false,
        songs: []
    }

    public componentDidMount() {
        this.fetchSongAssets().then(() => {
            this.setState({ ready: true })
        })
    }

    /** Loads next song and audio */
    private fetchSongAssets = async () => {
        const song = this.props.songs[this.state.currentSongIndex]
        document.title = `Now playing: ${song}`
        const audio = await import(
            /* webpackMode: "lazy-once" */
            `../../assets/audio/${song}.mp3`
        ).then((src) => src.default)
        const cover = await import(
            /* webpackMode: "lazy-once" */
            `../../assets/images/${song}.jpg`
        ).then((src) => src.default)

        this.setState({ currentSong: { audio, cover } })
    }

    /** Loads previous song */
    private handlePreviousSong = () => {
        this.setState(({ currentSongIndex }) => ({
            currentSongIndex: currentSongIndex - 1
        }), this.fetchSongAssets)
    }

    /** Loads next song */
    private handleNextSong = () => {
        if (this.state.currentSongIndex === this.props.songs.length - 1) {
            return
        }
        this.setState(({ currentSongIndex }) => ({
            currentSongIndex: currentSongIndex + 1
        }), this.fetchSongAssets)
    }

    private handleListClick = (song: string) => {
        const songIndex = this.props.songs.indexOf(song)
        this.setState({ currentSongIndex: songIndex }, this.fetchSongAssets)
    }

    private handleEnded = () => {
        if (this.props.autoplay === true) {
            this.handleNextSong()
        }
    }

    public render() {
        if (this.state.ready === false) {
            return <span>Loading...</span>
        }
        
        const { audio, cover } = this.state.currentSong!

        return (
            <React.Fragment>
                <Player
                    audio={audio}
                    cover={cover}
                    hasPreviousSong={this.state.currentSongIndex !== 0}
                    hasNextSong={this.state.currentSongIndex !== this.props.songs.length - 1}
                    onPrevious={this.handlePreviousSong}
                    onEnded={this.handleEnded}
                    onSkip={this.handleNextSong}
                />
                <SongList
                    songs={this.props.songs}
                    active={this.props.songs[this.state.currentSongIndex]}
                    onClick={this.handleListClick}
                />
            </React.Fragment>
        )
    }
}