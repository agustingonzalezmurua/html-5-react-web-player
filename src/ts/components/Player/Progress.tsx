import React, { memo } from "react"
const styles = require("~assets/components/player.scss")

interface Props {
    current: number
    duration: number
    onClick: (percetageClicked: number) => void
}

/**
 * Fills text with characters until the desierd length is achieved
 * @param str Text to parse
 * @param padString Text to fill
 * @param length desired length
 */
const lpad = function(str: string | number, padString: string, length: number) {
    while (String(str).length < length) {
        str = padString + str
    }
    return str;
}

const timestamp = function(duration: number) {
    if (isNaN(duration)) {
        return "00:00"
    }
    duration %= 3600
    const minutes = Math.floor(duration / 60)
    const seconds = duration % 60
    return `${lpad(minutes, "0", 2)}:${lpad(seconds, "0", 2)}`
}

const Progress = function({ current, duration, onClick }: Props) {
    const remaining = duration - current
    const progress = `${(current / duration) * 100}%`
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        if (!onClick) {
            return
        }
        const x = event.pageX - event.currentTarget.offsetLeft
        const clickedValue = x * (Number(event.currentTarget.dataset["max"])) / event.currentTarget.offsetWidth
        return onClick(clickedValue)
    }
    return (
        <section className={styles.progress}>
            <span role="elapsed time" className={styles.timestamp}>
                {timestamp(current)}
            </span>
            <section
                data-max="100"
                className={styles.progressbar}
                onClick={handleClick}
            >
                <span
                    className={styles.bar}
                    style={{ width: progress }}
                />
            </section>
            <span role="remaining time" className={styles.timestamp}>
                {timestamp(remaining)}
            </span>
        </section>
    )
}

export default memo(Progress)