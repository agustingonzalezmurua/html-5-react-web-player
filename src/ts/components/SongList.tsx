import React, { memo } from "react"
import classnames from "classnames"
const styles = require("~assets/components/songlist.scss")
interface Props {
    songs: string[],
    active: string,
    onClick?: (song: string) => void
}

const SongList = function ({ songs, active, onClick }: Props) {
    return (
        <div className={styles.container}>
            <ul className={styles.songList}>
                {
                    songs.map((song) =>
                        <li
                            className={
                                classnames(
                                    styles.songListItem, {
                                        [styles.songListItemActive]: song === active
                                    }
                                )
                            }
                            onClick={() => onClick && onClick(song)}
                            key={`song-${song}`}
                        >
                            {song}
                        </li>
                    )
                }
            </ul>
        </div>
    )
}

export default memo(SongList)