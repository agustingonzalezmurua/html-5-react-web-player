import React from "react"
import { hot } from "react-hot-loader/root"
import SongPlayer from "./components/SongPlayer";
import "@babel/polyfill";

const App = () => (
    <SongPlayer
        autoplay
        songs={[
            "tomorrow",
            "sad day",
            "slow motion"
        ]}
    />
)

export default hot(App)