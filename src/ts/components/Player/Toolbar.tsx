import React, { memo } from "react"
const styles = require("~assets/components/player.scss")

interface Props {
    playing: boolean
    onPlay?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    onPause?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    onPrevious?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    onSkip?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
    hasNextSong?: boolean
    hasPreviousSong?: boolean
}

const Toolbar = function({
    playing,
    onPlay,
    onPrevious,
    onPause,
    onSkip,
    hasNextSong,
    hasPreviousSong
}: Props) {
    return (
        <section className={styles.toolbar}>
            <button
                role="play previous track"
                onClick={onPrevious}
                className={styles.toolbarButton}
                disabled={!hasPreviousSong}
            >
                <i className="fas fa-step-backward"></i>
            </button>
            {
                (true === playing) ? (
                    <button
                        role="play current song"
                        onClick={onPause}
                        className={styles.toolbarButton}
                    >
                        <i className="fas fa-pause" />
                    </button>
                ) : (
                    <button
                        role="pause current song"
                        onClick={onPlay}
                        className={styles.toolbarButton}
                    >
                        <i className="fas fa-play" />
                    </button>
                )
            }
            <button
                role="play next track"
                onClick={onSkip}
                className={styles.toolbarButton}
                disabled={!hasNextSong}
            >
                <i className="fas fa-step-forward"></i>
            </button>
        </section>
    )
}

export default memo(Toolbar)