import React from 'react'
import "~assets/main.scss"

import ReactDOM from 'react-dom'
import { renderToString } from 'react-dom/server'
import App from '~ts/App'

ReactDOM.render(<App/>, document.getElementById('root'))

export default () => renderToString(<App/>)